import org.testng.Assert;
import org.testng.annotations.Test;

public class TestCalculator {
    Calculator calc = new Calculator();

    @Test
    public void TestSum() {
        Assert.assertEquals(calc.Sum(3.45, 6.55), 10.0);
    }

    @Test
    public void TestDiv() {
        Assert.assertNotEquals(calc.Div(25.5, 15), 10.5);
    }

    @Test
    public void TestSub() {
        Assert.assertEquals(calc.Div(100.2, 100.15), 0);
    }

    @Test
    public void TestMult() {
        Assert.assertFalse(calc.Sub(5.5, 4.4) == 0);
    }

}

